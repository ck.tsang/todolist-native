import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import tw from "twrnc";
import { faCheck } from "@fortawesome/free-regular-svg-icons"

const Task = (props) => {
  return (
    <View
      style={tw`flex flex-row justify-between items-center w-full rounded-md bg-white p-4 mt-6`}
    >
      <View style={tw`flex flex-row items-center`}>
        <TouchableOpacity
          style={tw`w-8 h-8 bg-blue-400 rounded-md bg-opacity-50`}
        ></TouchableOpacity>
        <Text style={tw`ml-4 text-lg`}>{props.text}</Text>
      </View>
      <View style={tw`h-5 w-5 bg-blue-400 rounded-full justify-center`}>
        <View style={tw`h-3 w-3 bg-white rounded-full self-center`}></View>
      </View>
    </View>
  );
};

export default Task;
