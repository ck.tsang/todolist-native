import React, { useState } from "react";
import { StatusBar } from "expo-status-bar";
import {
  Text,
  View,
  KeyboardAvoidingView,
  TextInput,
  TouchableOpacity,
  Keyboard,
} from "react-native";
import tw from "twrnc";
import Task from "./components/Task";

export default function App() {
  const [task, setTask] = useState();
  const [taskItems, setTaskItems] = useState([]);

  const handleAddTask = () => {
    Keyboard.dismiss();
    setTaskItems([...taskItems, task]);
    setTask(null);
  };

  const handleCompleteTask = (index) => {
    let itemsCopy = [...taskItems];
    itemsCopy.splice(index, 1);
    setTaskItems(itemsCopy);
  };

  return (
    <View style={tw`relative w-full h-full bg-blue-300`}>
      {/* Today's Tasks */}
      <View style={tw`my-20 mx-4`}>
        <Text style={tw`text-3xl font-bold`}>Today's Tasks</Text>

        {/* All Tasks */}
        <View style={tw`mt-4`}>
          {taskItems.map((item, index) => {
            return (
              <TouchableOpacity key={index} onPress={() => handleCompleteTask(index)}>
                <Task text={item} />
              </TouchableOpacity>
            );
          })}
        </View>
      </View>
      <StatusBar style="auto" />

      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={tw`absolute bottom-20 w-full justify-between flex flex-row`}
      >
        <TextInput
          style={tw`ml-4 self-center bg-white rounded-full py-2 px-6 h-10 w-72 text-center`}
          placeholder="Please write a task"
          value={task}
          onChangeText={(text) => setTask(text)}
        />
        <TouchableOpacity
          style={tw`h-10 w-10 rounded-full bg-white justify-center items-center mr-4`}
          onPress={handleAddTask}
        >
          <View>
            <Text style={tw` text-2xl`}>+</Text>
          </View>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    </View>
  );
}
